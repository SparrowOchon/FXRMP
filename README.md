[![pipeline status](https://gitlab.com/SparrowOchon/FXRMP/badges/master/pipeline.svg)](https://gitlab.com/SparrowOchon/FXRMP/commits/master)

# FirefoX Respect My Privacy

###### Purpose is to take the Privacy changes of Icecat and apply them to Firefox-Esr. This is targetted towards everyday firefox users and is NOT FOSS compliant as it uses Firefox-ESR instead of a rebuild like Icecat.

## What does this do ?

Allows users to have an up-to-date firefox-esr browser while benefiting from the Security from Firefox-ESR and its updating system. Auto-updating based on the latest firefox and latest Icecat automatically.

## Why not just use Icecat?

Icecat does provide great privacy but its really lacking in terms of security being numerous patches outdated.

## Not working with my OS?:

Currently I only have the path of the Activity-stream file on Debian. If you happend to use a different OS please feel free to make a PR with the
path to said file.

## Settings Effected:

    captivedetect.canonicalURL
    network.captive-portal-service.enabled
    extensions.shield-recipe-client.enabled
    browser.shell.checkDefaultBrowser
    extensions.autoDisableScopes
    extensions.shownSelectionUI
    ui.key.menuAccessKeyFocuses
    social.whitelist
    social.directories
    social.shareDirectory
    geo.enabled
    geo.wifi.uri
    security.ssl3.ecdhe_ecdsa_rc4_128_sha
    security.ssl3.ecdhe_rsa_rc4_128_sha
    security.ssl3.rsa_rc4_128_md5
    security.ssl3.rsa_rc4_128_sha
    browser.newtabpage.directory.source
    browser.newtabpage.directory.ping
    loop.enabled
    loop.feedback.baseUrl
    loop.gettingStarted.url
    loop.learnMoreUrl
    loop.legal.ToS_url
    loop.legal.privacy_url
    loop.oauth.google.redirect_uri
    loop.oauth.google.scope
    loop.server
    loop.soft_start_hostname
    loop.support_url
    loop.throttled2
    browser.pocket.enabled
    browser.pocket.api
    browser.pocket.site
    browser.pocket.oAuthConsumerKey
    browser.pocket.useLocaleList
    browser.pocket.enabledLocales
    browser.apps.URL
    browser.eme.ui.enabled
    media.eme.enabled
    media.eme.apiVisible
    media.gmp-manager.url.override
    media.gmp-provider.enabled
    media.gmp-gmpopenh264.provider.enabled
    browser.newtabpage.enhanced
    security.tls.version.min
    plugins.notifyMissingFlash
    network.http.pipelining
    network.http.proxy.pipelining
    network.http.pipelining.maxrequests
    nglayout.initialpaint.delay
    network.cookie.cookieBehavior
    general.appname.override
    general.appversion.override
    general.buildID.override
    general.oscpu.override
    general.platform.override
    privacy.donottrackheader.enabled
    privacy.donottrackheader.value
    dom.ipc.plugins.flash.subprocess.crashreporter.enabled
    browser.safebrowsing.enabled
    browser.safebrowsing.malware.enabled
    social.enabled
    social.remote-install.enabled
    datareporting.healthreport.uploadEnabled
    datareporting.healthreport.about.reportUrl
    datareporting.healthreport.documentServerURI
    healthreport.uploadEnabled
    social.toast-notifications.enabled
    datareporting.policy.dataSubmissionEnabled
    datareporting.healthreport.service.enabled
    browser.slowStartup.notificationDisabled
    network.http.sendRefererHeader
    network.http.referer.spoofSource
    dom.event.clipboardevents.enabled
    network.prefetch-next
    network.dns.disablePrefetch
    network.http.sendSecureXSiteReferrer
    toolkit.telemetry.enabled
    plugins.enumerable_names
    plugin.state.flash
    browser.search.update
    app.update.enabled
    app.update.auto
    privacy.announcements.enabled
    browser.snippets.enabled
    browser.snippets.syncPromo.enabled
    browser.snippets.geoUrl
    browser.snippets.updateUrl
    browser.snippets.statsUrl
    datareporting.policy.firstRunTime
    datareporting.policy.dataSubmissionPolicyVersion
    browser.webapps.checkForUpdates
    browser.webapps.updateCheckUrl
    security.ssl3.dhe_rsa_aes_128_sha
    security.ssl3.dhe_rsa_aes_256_sha
    keyword.URL
    browser.search.defaultenginename
    browser.search.order.1
    browser.search.defaultenginename
    browser.search.order.extra.duckduckgo
    browser.search.showOneOffButtons
    browser.search.suggest.enabled
    network.http.speculative-parallel-limit
    extensions.update.enabled
    intl.locale.matchOS
    browser.EULA.override
    extensions.blocklist.enabled
    startup.homepage_welcome_url
    browser.startup.homepage_override.mstone
    browser.dictionaries.download.url
    browser.search.searchEnginesURL
    layout.spellcheckDefault
    network.protocol-handler.app.apt
    network.protocol-handler.warn-external.apt
    network.protocol-handler.app.apt+http
    network.protocol-handler.warn-external.apt+http
    network.protocol-handler.external.apt
    network.protocol-handler.external.apt+http
    browser.safebrowsing.downloads.remote.enabled
    services.sync.privacyURL
    toolkit.telemetry.server
    experiments.manifest.uri
    toolkit.telemetry.unified
    dom.battery.enabled
    device.sensors.enabled
    camera.control.face_detection.enabled
    camera.control.autofocus_moving_callback.enabled
    network.http.speculative-parallel-limit
    browser.urlbar.userMadeSearchSuggestionsChoice
    browser.search.suggest.enabled
    browser.sessionstore.max_resumed_crashes
    browser.newtabpage.introShown
    browser.aboutHomeSnippets.updateUrl
    browser.sessionstore.max_resumed_crashes
    browser.safebrowsing.provider.mozilla.updateURL
    privacy.trackingprotection.enabled
    privacy.trackingprotection.pbmode.enabled
    privacy.trackingprotection.introURL
    browser.search.geoip.url
    privacy.resistFingerprinting
    webgl.disabled
    media.peerconnection.ice.no_host
    gecko.handlerService.schemes.mailto.0.name
    gecko.handlerService.schemes.mailto.1.name
    handlerService.schemes.mailto.1.uriTemplate
    gecko.handlerService.schemes.mailto.0.uriTemplate
    browser.contentHandlers.types.0.title
    browser.contentHandlers.types.0.uri
    browser.contentHandlers.types.1.title
    browser.contentHandlers.types.1.uri
    gecko.handlerService.schemes.webcal.0.name
    gecko.handlerService.schemes.webcal.0.uriTemplate
    gecko.handlerService.schemes.irc.0.name
    gecko.handlerService.schemes.irc.0.uriTemplate
    media.peerconnection.enabled
    media.peerconnection.ice.no_host
    media.peerconnection.ice.default_address_only
    gecko.handlerService.schemes.mailto.0.name
    gecko.handlerService.schemes.mailto.1.name
    handlerService.schemes.mailto.1.uriTemplate
    gecko.handlerService.schemes.mailto.0.uriTemplate
    browser.contentHandlers.types.0.title
    browser.contentHandlers.types.0.uri
    browser.contentHandlers.types.1.title
    browser.contentHandlers.types.1.uri
    gecko.handlerService.schemes.webcal.0.name
    gecko.handlerService.schemes.webcal.0.uriTemplate
    gecko.handlerService.schemes.irc.0.name
    gecko.handlerService.schemes.irc.0.uriTemplate
    extensions.blocklist.enabled
    font.default.x-western
    identity.mobilepromo.android
    keyword.URL
    browser.search.defaultenginename
    browser.search.order.extra.duckduckgo
    browser.search.showOneOffButtons
    browser.search.defaultenginename.US
    browser.search.order.US.1
    browser.search.order.US.2
    browser.search.order.US.3
    media.gmp-manager.url
    media.gmp-manager.url
    media.gmp-gmpopenh264.enabled
    media.gmp-eme-adobe.enabled
    middlemouse.contentLoadURL
    browser.selfsupport.url
    browser.preferences.inContent
    browser.newtabpage.introShown
    browser.aboutHomeSnippets.updateUrl
    gfx.direct2d.disabled
    browser.casting.enabled
    security.tls.unrestricted_rc4_fallback
    security.tls.insecure_fallback_hosts.use_static_list
    security.ssl.require_safe_negotiation
    security.ssl.treat_unsafe_negotiation_as_broken
    security.ssl3.rsa_seed_sha
    security.OCSP.enabled
    security.OCSP.require
    security.ssl3.dhe_dss_aes_128_sha
    security.ssl3.dhe_rsa_des_ede3_sha
    extensions.pocket.enabled
    xpinstall.signatures.required
    network.IDN_show_punycode
    extensions.screenshots.disabled
    browser.onboarding.newtour
    browser.onboarding.updatetour
    browser.onboarding.enabled
    browser.newtabpage.activity-stream.showTopSites
    browser.newtabpage.activity-stream.feeds.section.topstories
    browser.newtabpage.activity-stream.feeds.snippets
    browser.newtabpage.activity-stream.disableSnippets
    gfx.xrender.enabled
    dom.webnotifications.enabled
    dom.webnotifications.serviceworker.enabled
    dom.push.enabled

Note: Current firefox has a bug with the following [privacy.resistFingerprinting](https://bugzilla.mozilla.org/show_bug.cgi?id=1527747)
