FROM ubuntu:18.04

ENV TERM screen-256color
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
	git \
	python3 \
	python3-dev \
	python3-pip

RUN cd /opt && \
    git clone https://gitlab.com/SparrowOchon/FXRMP.git && \
    python3 /FXRMP/fxrmp.py
