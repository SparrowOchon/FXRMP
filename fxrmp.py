"""
FILENAME :      fxrmp

DESCRIPTION : Pull the latest privacy changes from Icecat specifically those made
                to the vendor.js and the settings.js file. Remove the settings
                specific to Icecat such as FOSS store and branding, and merge
                them with Firefox ESR by overwritting the global firefox
                settings file. As such this means that changes done to the
                user specific profile will override these settings.All previous
                Firefox-ESR settings will also be overwritting by this script.

                Usage:
                        python fxrmp.py


AUTHOR :   Network Silence        START DATE :    15 April 2019


License;
   Show don't Sell License Version 1
   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>

Copyright (c) 2019, Network Silence
All rights reserved.
"""
import os
import shutil
import sys
import platform
import tempfile
import re
from threading import Lock, Thread
import distro
import requests

LOCK = Lock()


def main():
    """
    @brief: Start of program responsible for calling and executing threads.
    """
    FILTER_LIST = [
        "app.releaseNotesURL",
        "app.vendorURL",
        "app.privacyURL",
        "plugins.hide_infobar_for_missing_plugin",
        "plugins.hide_infobar_for_outdated_plugin",
        "app.update.url",
        "general.useragent.compatMode.icecat",
        "app.support.baseURL",
        "app.support.inputURL",
        "app.feedback.baseURL",
        "browser.uitour.url",
        "plugins.update.url",
        "browser.customizemode.tip0.learnMoreUrl",
        "extensions.webservice.discoverURL",
        "extensions.getAddons.search.url",
        "app.faqURL",
        "pfs.datasource.url",
        "pfs.filehint.url",
        "browser.newtabpage.activity-stream.default.sites",
    ]
    ICECAT_SETTINGS_URL = (
        "https://git.savannah.gnu.org/cgit/gnuzilla.git/plain/data/settings.js"
    )
    ICECAT_VENDOR_URL = (
        "https://git.savannah.gnu.org/cgit/gnuzilla.git/plain/data/vendor.js"
    )
    ICECAT_ACTIVITY_STREAM = "icecat/browser/features/activity-stream@mozilla.org.xpi"

    try:
        FIREFOX_CONFIG_FILE, FIREFOX_ACTIVITY_STREAM = get_operating_system()

        VENDOR_THREAD = CreateThread(
            FIREFOX_CONFIG_FILE, ICECAT_VENDOR_URL, FILTER_LIST
        )
        VENDOR_THREAD.setName("Vendor Thread")
        VENDOR_THREAD.start()

        SETTING_THREAD = CreateThread(
            FIREFOX_CONFIG_FILE, ICECAT_SETTINGS_URL, FILTER_LIST
        )
        SETTING_THREAD.setName("Settings Thread")
        SETTING_THREAD.start()

        ACTIVITY_STREAM_THREAD = Thread(
            target=replace_file, args=(ICECAT_ACTIVITY_STREAM, FIREFOX_ACTIVITY_STREAM)
        )
        ACTIVITY_STREAM_THREAD.setName("Activity Stream Thread")
        ACTIVITY_STREAM_THREAD.start()

        VENDOR_THREAD.join()
        SETTING_THREAD.join()
        ACTIVITY_STREAM_THREAD.join()
    except ValueError as err:
        print(str(err))
        sys.exit()


def get_operating_system():
    """
    @brief Identify users Operating System and return the firefox global config.

    @return {String} Path to file containing the firefox user settings
    """
    if platform.system() == "Linux":
        if distro.id() == "debian":
            return (
                "/etc/firefox-esr/firefox-esr.js",
                "/usr/lib/firefox-esr/browser/features/",
            )
        elif distro.id() == "gentoo" or distro.id() == "arch":
            return "/usr/lib64/firefox-esr/mozilla.cfg"
        else:
            return "/etc/firefox-esr/syspref.js"
    elif platform.system() == "Darwin":
        return "/Applications/Firefox.app/Contents/Resources/mozilla.cfg"
    elif platform.system() == "Windows":
        return r"C:\Program Files (x86)\Mozilla Firefox\mozilla.cfg"
    else:
        raise ValueError("Platform not supported!")


def replace_file(source_file, dest_file_location):
    """
    @brief Replace a file on the system with a file we predefine
    """
    xpi_file = f"{dest_file_location}{source_file.split('/')[-1]}"
    if os.path.exists(xpi_file):
        os.remove(xpi_file)
    elif not os.path.exists(dest_file_location):
        raise Exception(f"Invalid Directory: {dest_file_location}")
    else:
        shutil.move(os.getcwd() + "/" + source_file, dest_file_location)



def get_url_file(source_url):
    """
        @brief Fetch the file being passed in as a parameter and store it in
            tmp file.

        @param
            source_url {String} Url of the file we want to download

        @return {String} Localpath of the newly downloaded file
    """
    try:
        temp_file = (tempfile.mkstemp())[1]
        fetched_file = requests.get(source_url, allow_redirects=True)
        with open(temp_file, "wb") as opened_temp_file:
            opened_temp_file.write(fetched_file.content)
    except (FileNotFoundError, IOError) as err:
        print(str(err))

    return temp_file


def regex_file_filter(source_file, setting_string):
    """
        @brief Create a list of of items from a file based on a passed
                Regex string.

        @para
            source_file {String} File Path which we want to filter lines from.
            settings_file {Object} Compiled Regex which will filter the file.


        @return {List} A list of matches of the passed setting_string regex

    """
    try:
        with open(source_file, "r", encoding="utf-8") as opened_source_file:
            original_settings = setting_string.findall(opened_source_file.read())
            return original_settings
    except (FileNotFoundError, IOError) as err:
        print(str(err))


def remove_filter_matches(source_file, filter_list):
    """
    @brief Remove all lines matching a entry in a predefined FILTER_LIST
            from the source_file

    @param
        source_file {String} Path to file whose data we want to filter
        FILTER_LIST {Array} List of rules to filter by
    """
    try:
        with open(source_file, "r", encoding="utf8") as opened_source_file:
            read_file = opened_source_file.read()
        for removable_filter in filter_list:
            read_file = re.sub(r'pref\("' + removable_filter + '".*', "", read_file)
        with open(source_file, "w", encoding="utf-8") as opened_source_file:
            opened_source_file.write(read_file)
    except (FileNotFoundError, IOError) as err:
        print(str(err))


def merge_unique_entries(source_file, dest_file):
    """
    @brief Take unique lines from source_file and append them
            to dest_file
    @param
     source_file {String} Path to file we want to pull data from
     dest_file {String} Path to file who we want to append to
    """
    setting_string = re.compile(r'^(?=(?!//))(?=pref\("(.*?)")', re.M)
    LOCK.acquire()
    try:
        original_settings = regex_file_filter(dest_file, setting_string)
    finally:
        LOCK.release()
    with open(source_file, "r", encoding="utf-8") as opened_source_file:
        for line in opened_source_file:
            if setting_string.search(line) == None:
                continue
            source_setting = setting_string.search(line).group(1)
            if source_setting not in original_settings:
                LOCK.acquire()
                try:
                    with open(dest_file, "a+", encoding="utf-8") as opened_dest_file:
                        opened_dest_file.write(line)
                finally:
                    LOCK.release()


class CreateThread(Thread):
    """
    @brief Thread Class responsible for filtering vendor and settings
            files as threads.
    """

    def __init__(self, FIREFOX_CONFIG_FILE, URL_PATH, FILTER_LIST):
        Thread.__init__(self)
        self.firefox_config_file = FIREFOX_CONFIG_FILE
        self.url_path = URL_PATH
        self.filter_list = FILTER_LIST

    def run(self):
        fetched_file = get_url_file(self.url_path)
        remove_filter_matches(fetched_file, self.filter_list)
        merge_unique_entries(fetched_file, self.firefox_config_file)


if __name__ == "__main__":
    main()
